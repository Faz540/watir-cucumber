

# Example Frontend Automation using WatiR - RSpec Framework:
#### Author: 
Paul Joseph Farrell
#### Email: 
pauljosephfarrell89@gmail.com
#### LinkedIn: 
[Feel free to add me on Linked In](https://www.linkedin.com/in/faz540/)
#### The Objective Of This Repository:
The aim of this repository is to act as a tutorial for fellow testers (mostly for testers in the 'Global Tech Quality SE 2' WhatsApp group) that want to start writing frontend automation.
The second aim is to act as a 'portfolio' to demonstrate how I write frontend automation for any future employers. 
#### Quick Bio:  
I started my testing career in 2012 where I was lucky enough to be a Functionality Tester for Sony PlayStation for over 2 years.

In 2014, I joined a small IT company known as InventiveIT, which later grew signficantly and became ADVAM UK.
It was at Inventive IT/ ADVAM UK where I learnt most of my skills.(Cross browser and device testing, API testing using Postman and I dabbled a little in the world of frontend automation using Ruby.

I had a great career at InventiveIT/ADVAM UK but decided to take a risk and leave my comfort zone and challenge myself by joining Beauty Bay, an eCommerce company specialising in cosmetics, in 2016.

At BeautyBay, I'm able to focus a lot more on web automation using Javascript(API/Frontend).


## Prerequisite/ Installation:

```
gem install watir
gem install watir-scroll
gem install cucumber
gem install rspec-expectations
gem install page-object
```