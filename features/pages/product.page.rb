class ProductPage

    def initialize(browser)
        @browser = browser
    end

    def visit
        @browser.goto('http://automationpractice.com/index.php?id_product=1&controller=product')
    end

    def add_item_cart
        @browser.button(name: 'Submit').click
        @browser.div(id: 'layer_cart').wait_until_present
    end

    def get_cart_popup_text
        @browser.div(id: 'layer_cart').text
    end
    
end