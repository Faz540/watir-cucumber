class HomePage

    def initialize(browser)
        @browser = browser
    end
       
    def visit
        @browser.goto('http://automationpractice.com/index.php')
    end
    
    def search_for(query)
        search_field = @browser.text_field(id: 'search_query_top')
        search_field.clear
        search_field.set(query)
        search_button = @browser.button(class: 'button-search')
        search_button.click
    end

end