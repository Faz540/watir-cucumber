class ResultsPage

    def initialize(browser)
        @browser = browser
    end

    def number_of_results
        results = @browser.span(class: 'heading-counter').text[0]
        results.to_i
    end
    
end