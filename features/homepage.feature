Feature: HomePage Tests

    Scenario: I am able to perform a successful search
        Given I am on the HomePage
        When I search for 'black dresses'
        Then At least 1 result should be returned

    Scenario: Perform the above scenario, but using Page Objects
        Given I am on the HomePage using Page Objects
        When I search for 'black dresses' using Page Objects
        Then At least 1 result should be returned using Page Objects

