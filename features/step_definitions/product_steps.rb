Given("I am on a Product Page") do
    @product_page = ProductPage.new(@browser)
    @product_page.visit
    expect(@browser.title).to eq('Faded Short Sleeve T-shirts - My Store')
end

When("I click the Add To Bag button") do
    @product_page.add_item_cart
end

Then("the product should successfully be added to the user's cart") do
    expect(@product_page.get_cart_popup_text).to include('Product successfully added to your shopping cart')
end