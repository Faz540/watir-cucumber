Given("I am on the HomePage") do
  @browser.goto'http://automationpractice.com/index.php'
  expect(@browser.title).to eq('My Store')
end

Given("I search for {string}") do |string|
  @browser.text_field(id: 'search_query_top').clear
  @browser.text_field(id: 'search_query_top').set(string)
  @browser.button(class: 'button-search').click
  expect(@browser.title).to eq('Search - My Store')
end

Then("At least {int} result should be returned") do |int|
  numberOfItems = @browser.span(class: 'heading-counter').text[0]
  numberOfItems = numberOfItems.to_i
  expect(numberOfItems).to be >= int
end

Given("I am on the HomePage using Page Objects") do
  @home_page = HomePage.new(@browser)
  @home_page.visit
  expect(@browser.title).to eq('My Store')
end

Given("I search for {string} using Page Objects") do |string|
  @home_page.search_for(string)
  expect(@browser.title).to eq('Search - My Store')
end

Then("At least {int} result should be returned using Page Objects") do |int|
  @results_page = ResultsPage.new(@browser)
  expect(@results_page.number_of_results).to be >= int
end