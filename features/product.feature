Feature: Product Page Tests

    Scenario: I am able to add an item to the cart
        Given I am on a Product Page
        When I click the Add To Bag button
        Then the product should successfully be added to the user's cart
